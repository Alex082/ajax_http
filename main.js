//class для создания и удаления кмрточки
class Card {
  constructor(post) {
    this.id = post.id;
    this.title = post.title;
    this.body = post.body;
    this.user = post.user;
  }

  create() {
    const card = document.createElement("div");
    card.className = "card";
    card.innerHTML = `<h3>${this.title}</h3>
        <p>${this.body}.</p>
        <p>Автор: ${this.user.name}, ${this.user.username}, Email: (${this.user.email}).</p>
        <span class="delete-button" post-id="${this.id}">Delete</span>
        `;

    const deleteButton = card.querySelector(".delete-button");
    deleteButton.addEventListener("click", this.delete.bind(this));

    return card;
  }

  delete(event) {
    let idPost = event.target.getAttribute("post-id");

    fetch(`https://ajax.test-danit.com/api/json/posts/${idPost}`, {
      method: "DELETE",
    }).then((response) => {
      if (response.ok) {
        event.target.parentNode.remove();
      } else {
        console.log("Карточка не удалилась.");
      }
    });
  }
}

//class для информацыи находящaйся в кмрточке

class newsFeed {
  constructor() {
    this.users = new Map();
    this.posts = [];
    this.feed = document.getElementById("newsFeed");
  }

  load() {
    Promise.all([this.getUsers(), this.getPosts()]).then(() => {
      this.displayPosts();
    });
  }

  //получаем юзер
  getUsers() {
    return fetch(`https://ajax.test-danit.com/api/json/users`)
      .then((response) => response.json())
      .then((users) => {
        console.log(users);
        users.forEach((user) => {
          this.users.set(user.id, user);
        });
      });
  }

  //получаем пост
  getPosts() {
    return fetch(`https://ajax.test-danit.com/api/json/posts`)
      .then((response) => response.json())
      .then((posts) => {
        // console.log(posts);
        this.posts = posts;
      });
  }

  displayPosts() {
    this.feed.innerHTML = " ";

    this.posts.forEach((post) => {
      let user = this.users.get(post.userId);
      post.user = {
        name: user.name,
        username: user.username,
        email: user.email,
      };

      const card = new Card(post);
      const cardElement = card.create();
      this.feed.appendChild(cardElement);
    });
  }
}

//вызываем
const newFeed = new newsFeed();
document.addEventListener("DOMContentLoaded", () => {
    newFeed.load()
});
